-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.24-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             7.0.0.4389
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table base-igniter.user
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('admin','client') NOT NULL DEFAULT 'client',
  `alias` varchar(16) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(64) NOT NULL,
  `key` varchar(64) NOT NULL,
  `banned` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `activated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `login_fail` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `date_login_fail` datetime NOT NULL,
  `date_login` datetime NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table base-igniter.user: ~7 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`user_id`, `type`, `alias`, `email`, `password`, `key`, `banned`, `activated`, `login_fail`, `date_login_fail`, `date_login`, `date_added`) VALUES
  (1, 'admin', '', 'admin@admin.com', '9c364cf790faf55e9aa9c9c3be76bb50c39b27113c845debe5b7793f34295f52', 'e0551ae202309e8863f03eb5fceddaa81335aed13f1c4696b50d907b3fdca62d', 0, 0, 0, '0000-00-00 00:00:00', '2013-07-28 05:14:13', '0000-00-00 00:00:00'),
  (2, 'client', 'jesse', 'c@c.com', '9c364cf790faf55e9aa9c9c3be76bb50c39b27113c845debe5b7793f34295f52', 'e0551ae202309e8863f03eb5fceddaa81335aed13f1c4696b50d907b3fdca62d', 0, 0, 0, '0000-00-00 00:00:00', '2013-07-28 05:14:13', '0000-00-00 00:00:00'),
  (3, 'client', '', 'd@d.com', '9c364cf790faf55e9aa9c9c3be76bb50c39b27113c845debe5b7793f34295f52', '6a1a11ecb46bf755bee282d96773630bb1b876ae8cdfaef4c39bbf78a5bad1c0', 0, 1, 0, '0000-00-00 00:00:00', '2013-07-28 05:14:13', '2013-06-23 23:14:27'),
  (4, 'client', '', 'a@a.com', '9c364cf790faf55e9aa9c9c3be76bb50c39b27113c845debe5b7793f34295f52', '589fe0398fdd4945eb847216083180b2b0a6c9d6a2291627b20863d6169349ee', 0, 1, 0, '0000-00-00 00:00:00', '2013-07-28 05:14:13', '2013-06-23 23:16:57'),
  (5, 'client', '', 'r@r.com', '9c364cf790faf55e9aa9c9c3be76bb50c39b27113c845debe5b7793f34295f52', 'd2349658b4a11f5ef670ae7791a9488340689aeac0639e4bbf1d9fc38c7be1e0', 0, 1, 0, '0000-00-00 00:00:00', '2013-07-28 05:14:13', '2013-06-23 23:18:07'),
  (6, 'client', '', 's@s.com', '9c364cf790faf55e9aa9c9c3be76bb50c39b27113c845debe5b7793f34295f52', 'db6374518f6f602c60e5f4c4ce5209a1fc4ed94095e369e7eeb03688139f6432', 0, 1, 0, '0000-00-00 00:00:00', '2013-07-28 05:14:13', '2013-06-23 23:18:39'),
  (7, 'client', '', 'o@o.com', '9c364cf790faf55e9aa9c9c3be76bb50c39b27113c845debe5b7793f34295f52', '79079d085b7c9bcdcc8999cb689c6c7b20c89959342a1f4d625835371a3e30f4', 0, 1, 0, '0000-00-00 00:00:00', '2013-07-28 05:14:13', '2013-06-23 23:20:53');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;


-- Dumping structure for table base-igniter.user_meta
CREATE TABLE IF NOT EXISTS `user_meta` (
  `meta_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `group` varchar(50) NOT NULL,
  `key` varchar(50) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`meta_id`),
  UNIQUE KEY `user_id_key_type` (`user_id`,`key`,`group`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table base-igniter.user_meta: ~5 rows (approximately)
/*!40000 ALTER TABLE `user_meta` DISABLE KEYS */;
INSERT INTO `user_meta` (`meta_id`, `user_id`, `group`, `key`, `value`) VALUES
  (1, 2, 'profile', 'first_name', 'jesse'),
  (2, 2, 'profile', 'last_name', 'boyer'),
  (3, 1, 'profile', 'first_name', 'jesse'),
  (4, 1, 'profile', 'last_name', 'boyer');
/*!40000 ALTER TABLE `user_meta` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
