<?=$header?>

<div class="container">

<p class="lead">
Welcome to Base-Igniter. This system is built with nested controllers and organized
models to make jump-starting CodeIgniter a lot easier.
</p>

<p>
This system does <strong>not modify core files</strong> and opposes such a thing!
The goal is to keep CI as intact as possible and make the application a bit more
flexible.
</p>

<h3>Features Include</h3>
<ul>
    <li>CRUD Model</li>
    <li>META Model</li>
    <li>Nested Client Controllers</li>
    <li>Nested Client Views</li>
    <li>Nested Admin Controllers</li>
    <li>Nested Admin Views</li>
    <li>Nested Shared Views</li>
    <li>User Ready Login and Profile</li>
    <li>Admin Ready Login and Profile</li>
    <li>Preset Database Configuration (schema.sql)</li>
</ul>

</div>

<?=$footer?>