<?=$header;?>

<form class="form-horizontal" method="post" action="<?=site_url('admin/do_account_setting')?>">
    <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
    <input type="hidden" name="user_id" value="<?=@$account[0]['user_id']?>">

    <div class="control-group">
      <label class="control-label">Email</label>
      <div class="controls">
        <input type="text" class="input-medium" name="email" placeholder="" value="<?=@$account[0]['email']?>">
      </div>
    </div>
    <div class="control-group">
      <label class="control-label">Password</label>
      <div class="controls">
        <input type="password" class="input-medium" name="password" placeholder="" >
      </div>
    </div>
    <div class="control-group">
      <label class="control-label">Confirm Password</label>
      <div class="controls">
        <input type="password" class="input-medium" name="confirm_password" placeholder="">
      </div>
    </div>
  <div class="control-group">
      <div class="controls">
          <input type="submit" value="Update" class="btn">
      </div>
  </div>

</form>


<?=$footer;?>