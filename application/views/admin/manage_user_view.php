<?=$header;?>

<div class="span11">
    <a href="#" class="pull-right btn btn-primary">New User</a>

    <div class="clearfix"></div>

    <hr />

    <table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>#</th>
        <th>Email</th>
        <th>Options</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($client as $c): ?>
        <tr>
            <td><?=$c['user_id']?></td>
            <td><?=$c['email'];?></td>
            <td>
                <a href="<?=site_url('admin/do_ban')?>" data-bool="<?=!$c['banned']?>" data-user="<?=$c['user_id']?>" class="do_ban btn btn-small"><?=($c['banned'] == 0) ? 'Ban' : 'Unban';?></a>
                <a href="<?=site_url('admin/do_activate')?>" data-bool="<?=$c['activated']?>" data-user="<?=$c['user_id']?>" class="do_activate btn btn-small"><?=($c['activated'] == 0) ? 'Activate' : 'Deactivate';?></a>
                <a href="<?=site_url('admin/do_delete')?>" data-user="<?=$c['user_id']?>" class="do_delete btn btn-small">Delete</a>
            </td>
        </tr>
    <?php endforeach;?>
    </tbody>
    </table>
</div>

<script type="text/javascript">
$(function() {

    $(".do_ban, .do_activate, .do_delete").click(function(e) {
        e.preventDefault();

        var c = confirm('Are you sure you want to?');
        if (c == false) return false;

        var self = $(this);
        var url = $(this).attr('href');
        var postData = {};
        postData.user_id = $(this).data('user');
        postData.bool = $(this).data('bool');
        postData['<?=$this->security->get_csrf_token_name()?>'] = '<?=$this->security->get_csrf_hash()?>';

        $.post(url, postData, function(o) {
            if (o.result == 1) {
                Result.success('Successful.');
            } else {
                Result.error(o.error);
            }
        }, 'json');
    });

});
</script>


<?=$footer;?>