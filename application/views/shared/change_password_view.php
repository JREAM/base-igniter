<?=$header?>

<div class="title">
    <h3>Change Password</h3>
</div>

<div class="span4">

    <div class="account-container register stacked">
        <div class="content clearfix">

            <?php if ($has_valid_key):?>
            <form id="change_password_form" action="<?=site_url('api_user/do_change_password')?>" method="post" class="form-horizontal">
                <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
                <div class="control-group">
                    <label class="control-label">Password</label>
                    <div class="controls">
                        <input type="password" name="password" value="" placeholder="Password" class="login" />
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Confirm Password</label>
                    <div class="controls">
                        <input type="password" name="confirm_password" value="" placeholder="Confirm Password" class="login" />
                    </div>
                </div>

                <input type="hidden" name="key" value="<?=$key?>" />
                <input type="hidden" name="user_id" value="<?=$user_id?>" />

                <div class="control-group">
                    <div class="ajax-loader hide pull-right text-center">
                        Please Wait<br />
                        <img class="pull-right" src="<?=site_url('public/img/ajax-loader.gif')?>" alt="Loading" />
                    </div>
                    <div class="controls">
                        <input type="submit" class="submit-btn button btn btn-primary btn-large" value="Submit" />
                    </div>
                </div>

            </form>

            <?php else:?>
                <h1>Invalid Key</h1>
                <p>
                Please use the link supplied in your email address.
                </p>
            <?php endif;?>
        </div> <!-- /content -->

    </div> <!-- /account-container -->

    <!-- Text Under Box -->
    <div class="login-extra">
        Already have an account? <a href="<?=site_url('client/login')?>">Login</a>
    </div> <!-- /login-extra -->

</div>

<script>
$(function() {
    $("#change_password_form").submit(function(e) {
        e.preventDefault();

        $(".ajax-loader").removeClass('show');
        $(".submit-btn").addClass('hide');

        var url = $(this).attr('action');
        var postData = $(this).serialize();

        $.post(url, postData, function(o) {
            if (o.result == 1) {
                Result.success('Your password has been reset, you may now login.');
                $(".content").html('<h1>Please Wait</h1><p>Your password has been changed, please <a href="<?=site_url("home/index")?>">Login</a> with your new password.</p>');

            } else {
                $(".ajax-loader").addClass('hide');
                $(".submit-btn").removeClass('hide');

                Result.error(o.error);
            }
        }, 'json')

    })
});
</script>

<?=$footer?>