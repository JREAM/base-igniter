<?=$header?>

<div class="title">
    <h3><?=$title?></h3>
</div>

<div class="span4 clearfix">
    <div class="account-container login stacked">

        <div class="content clearfix">

            <form id="login_form" class="form-horizontal" action="<?=site_url('api_user/do_login')?>" method="post">
                <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
                <input type="hidden" name="user_type" value="<?=$user_type?>" />

                <div class="control-group">
                    <label class="control-label">Email</label>
                    <div class="controls">
                        <input type="text" id="login" name="email" value="" placeholder="Email" class="login username-field" />
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Password</label>
                    <div class="controls">
                        <input type="password" id="password" name="password" value="" placeholder="Password" class="login password-field"/>
                    </div>
                </div>

                <div class="control-group">
                    <div class="ajax-loader hide pull-right text-center">
                        Please Wait<br />
                        <img class="pull-right" src="<?=site_url('public/img/ajax-loader.gif')?>" alt="Loading" />
                    </div>
                    <div class="controls">
                        <button class="sign-in button btn btn-primary">Sign In</button>
                    </div>
                </div>

            </form>

        </div> <!-- /content -->

    </div> <!-- /account-container -->

    <?php if ($user_type != 'admin'):?>
        <div class="login-extra">
            <a href="<?=site_url('client/login/register')?>">Sign Up</a> |
            <a href="<?=site_url('client/login/forgot_password')?>">Forgot Password</a>
        </div>
    <?php endif;?>
</div>

<script>
$(function() {

    $("#login_form").submit(function(e) {
        e.preventDefault();

        $(".sign-in").addClass('hide');
        $(".ajax-loader").removeClass('hide');

        var url = $(this).attr('action');
        var postData = $(this).serialize();

        $.post(url, postData, function(o) {
            if (o.result == 1) {
                // Redirect to the proper location based on the return user type
                window.location.href = '<?=site_url()?>' + o.data.redirect;
            } else {
                $(".ajax-loader").addClass('hide');
                $(".sign-in").removeClass('hide');

                Result.error(o.error.message);
            }
        })

    })
});
</script>


<?=$footer?>