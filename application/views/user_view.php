<?=$header?>

<div class="container">

List out users

<ul>
<?php foreach($user as $u):?>
    <li>
        <?php if (!empty($u['alias'])):?>
            <?php $url = 'user/' . strtolower($u['alias']);?>
            <a href="<?=site_url($url)?>"><?=$u['alias'];?></a>
        <?php else:?>
            <em>No Name</em>
        <?php endif;?>
    </li>
<?php endforeach;?>
</ul>

</div>

<?=$footer?>