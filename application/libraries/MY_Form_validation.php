<?php

class MY_Form_validation extends CI_Form_validation
{

    public function __construct($config = array())
    {
        parent::__construct($config);
    }   

    /**
     * Extend the Validation Class so I can return an array of errors
     * 
     * @return array
     */
    public function error_array()
    {
        if (count($this->_error_array) > 0)
        return $this->_error_array;
    }
}