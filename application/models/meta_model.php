<?php
/**
 * @author      Jesse Boyer <contact@jream.com>
 * @copyright   Copyright (C), 2013 Jesse Boyer
 * @license     GNU General Public License 3 (http://www.gnu.org/licenses/)
 *              Refer to the LICENSE file distributed within the package.
 *
 * @version     0.3
 * @link        http://jream.com
 *
 * @help        See data.sql and readme.md
 *
 */

class Meta_model extends CI_Model {

    // ------------------------------------------------------------------------

    protected $_meta_table      = null;
    protected $_parent_table    = null;
    protected $_reference_key   = null;

    // ------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();
    }

    // ------------------------------------------------------------------------

    /**
     * init (Required) before running, sets the tables
     *
     * @param string $table Name of the main table (eg: user, file)
     *                   Your meta table will automaticaly be $tablename_meta
     * @param string $reference_key (optional) Name of the reference
     *                   key between both tables. If this is not set
     *                   the default $tablename_id will be used.
     */
    public function init($table, $reference_key = false)
    {
        $this->_parent_table  = $table;
        $this->_meta_table    = $table . '_meta';
        $this->_reference_key = ($reference_key == false) ? $table . '_id' : $reference_key;
    }

    // ------------------------------------------------------------------------

    /**
     * Gets arbitrary set of data
     *
     * @param string   $group
     * @param string   $key
     * @param integer  $user_id
     *
     * @return string
     */
    public function get($group, $key = false, $reference_id = false)
    {
        $this->db->where('group', $group);

        if ($key) {
            $this->db->where('key', $key);
        }

        if ($reference_id) {
            $this->db->where($this->_reference_key, $reference_id);
        }

        $query  = $this->db->get($this->_meta_table);

        if ($key)
        {
            // Get a single row
            $data = $query->row_array();
            if (empty($data)) {
                return false;
            }
            return $data['value'];
        }
        else
        {
            $data = $query->result_array();
            $new_data = array();
            foreach ($data as $_key => $_value)
            {
                $key_name = $_value['key'];
                $group = $_value['group'];

                // Construct an array if needed
                if (!isset($data[$group]))
                {
                    $data[$group] = array();
                }

                $new_data[$group][$key_name] = $_value['value'];
            }
            return $new_data;
        }

        return false;
    }

    // ------------------------------------------------------------------------

    /**
     * Sets an arbitrary data to a user
     *
     * @param string   $group
     * @param string   $key
     * @param string   $value
     * @param integer  $user_id
     *
     * @return integer
     */
    public function set($group, $key, $value, $reference_id = false)
    {

        // Meet the first criteria
        if ($reference_id) {
            $this->db->where($this->_reference_key, $reference_id);
        }

        $this->db->where('group', $group);
        $this->db->where('key', $key);
        $query  = $this->db->get($this->_meta_table);
        $row_count = $query->num_rows();

        // Meet the second criteria
        if ($reference_id) {
            $this->db->where($this->_reference_key, $reference_id);
        }

        if ($row_count == 0)
        {
            // Insert
            $result = $this->db->insert($this->_meta_table, array(
                $this->_reference_key => $reference_id,
                'group' => $group,
                'key' => $key,
                'value' => $value
            ));

            $output = $this->db->insert_id();
        }
        else
        {
            // Update
            $this->db->where('group', $group);
            $this->db->where('key', $key);
            $this->db->update($this->_meta_table, array('value' => $value));

            $output = $this->db->affected_rows();
        }

        return $output;
    }

    // ------------------------------------------------------------------------

    /**
     * Gets all or many records based on the set parameters of the parent table
     * This includes the parent class
     *
     * @param array|integer  $params (Optional)
     *                          Array Key/Value pair for parent table
     *                          Integer matches the reference_key
     *
     * @return array  An array returns Enumerated Array, integer returns Associative Array
     */
    public function get_bundle($param = array())
    {
        // Flag the return type
        $return_type = 'enum';

        // Based on custom parameters
        if (is_array($param))
        {
            foreach ($param as $_key => $_value)
            {
                $this->db->where($_key, $_value);
            }
        }

        // Based on reference key
        if (is_numeric($param))
        {
            $this->db->where($this->_reference_key, $param);
            $return_type = 'assoc';
        }

        // Grab The Parent Table
        $query  = $this->db->get($this->_parent_table);
        $data   = $query->result_array();

        // Loop the parent tables keys
        foreach($data as $_key => $_value)
        {
            // Grab the User Data
            $this->db->where($this->_reference_key, $_value[$this->_reference_key]);
            $query  = $this->db->get($this->_meta_table);
            $meta_data = $query->result_array();

            // Build the meta data
            foreach ($meta_data as $__key => $__value)
            {
                $key_name = $__value['key'];
                $group = $__value['group'];

                // Create an array if needed
                if (!isset($data[$_key][$group]))
                {
                    $data[$_key][$group] = array();
                }

                $data[$_key][$group][$key_name] = $__value['value'];
            }
        }

        if ($return_type == 'assoc') {
            return $data[0];
        }

        return $data;
    }

    // ------------------------------------------------------------------------

    /**
     * Delete a meta record based on the group, key and reference_id
     *
     * @param string $group
     * @param string $key
     * @param integer $reference_id
     *
     * @return integer Affected rows
     */
    public function delete($group, $key, $reference_id)
    {
        $this->db->where($this->_reference_key, $reference_id);
        $this->db->where('group', $group);
        $this->db->where('key', $key);
        $this->db->delete($this->_meta_table);

        return $this->db->affected_rows();
    }

    // ------------------------------------------------------------------------

    /**
     * Delete a group of meta record based on the group and reference_id
     *
     * @param string $group
     * @param integer $reference_id
     *
     * @return integer Affected rows
     */

    public function delete_group($group, $reference_id = false)
    {
        $this->db->where($this->_reference_key, $reference_id);
        $this->db->where('group', $group);
        $this->db->delete($this->_meta_table);

        return $this->db->affected_rows();
    }

    // ------------------------------------------------------------------------

}