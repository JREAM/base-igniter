<?php

class User_model extends CRUD_model {

    // ------------------------------------------------------------------------

    protected $_table    = 'user';
    protected $_primary_key = 'user_id';

    // ------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();

        // Load a fresh instance of the Meta Model
        if (!class_exists('Meta_model')) {
            require APPPATH . 'models/meta_model.php';
        }
        $this->meta = new Meta_model();
        $this->meta->init('user');
    }

    // ------------------------------------------------------------------------

    /**
    * Returns the state of a user property
    *
    * @param integer $user_id
    *
    * @return array Associative
    */
    private function _get_state($user_id)
    {
        $this->db->select('banned, activated');
        $this->db->where('user_id', $user_id);
        $query  = $this->db->get('user');
        $data   = $query->row_array();

        return $data;
    }

    // ------------------------------------------------------------------------

    /**
     * Is the user activated?
     *
     * @param integer  $user_id
     *
     * @return boolean
     */
    public function is_activated($user_id)
    {
        $this->_get_state($user_id);
        return $data['activated'];
    }

    // ------------------------------------------------------------------------

    /**
     * Is the user banned?
     *
     * @param integer  $user_id
     *
     * @return boolean
     */
	public function is_banned($user_id)
	{
        $this->_get_state($user_id);
        return $data['banned'];
	}

    // ------------------------------------------------------------------------

    /**
    * Logs a user in
    *
    * @param string $email
    * @param string $password
    * @param string $type
    *
    * @return array
    *           code
    *           message
    */
    public function login($email, $password, $type)
    {
        $password_hash = hash('sha256', $password . $this->config->item('encryption_key'));

        // Get the user with the associated email login
        $this->db->select('user_id, password, activated, type, banned, login_fail, date_login_fail');
        $result = $this->get(array(
            'email' => $email,
        ));

        // If no email was found
        if (empty($result)) {
            return array(
                'code' => 0,
                'message' => 'No user exists with these credentials.'
            );
        }

        // Make sure there are not too many login attempts on this account
        $elapsed = strtotime(DATETIME) - strtotime($result[0]['date_login_fail']);
        $login_fail = $result[0]['login_fail'];

        if ($login_fail == 3 && $elapsed < 30) {
            $remaining = 30 - $elapsed;
            return array(
                'code' => 30,
                'message' =>
                    "Timed out for 30 seconds due to too many login attempts." .
                    "$remaining seconds remaining."
            );
        }

        if ($login_fail >= 10 && $elapsed < 600) {
            $remaining = 600 - $elapsed;
            return array(
                'code' => 100,
                'message' =>
                    "Timed out for 10 minutes due to too many login attempts. " .
                    "$remaining seconds remaining."
            );
        }

        // If the user type is not a match
        if ($type != $result[0]['type']) {
            return array(
                'code' => 5,
                'message' => 'No user exists with these credentials.'
            );
        }

        // If they are at the password point and it does not match,
        // increment the failure count and failure date
        if ($result[0]['password'] !== $password_hash)
        {
            $this->update(array(
                'login_fail' => ++$result[0]['login_fail'],
                'date_login_fail' => DATETIME
            ), array('email' => $email));

            return array(
                'code' => 0,
                'message' => 'No user exists with these credentials.'
            );
        }

        // Get the user_id
        $user_id    = $result[0]['user_id'];
        $activated  = $result[0]['activated'];

        // Make sure a Client isn't trying to login as an Admin
        if ($result[0]['type'] != $type) {
            return array(
                'code' => 10,
                'message' => 'No user exists with these credentials.'
            );

        }

        // Make sure they are not banned
        if ($result[0]['banned'] == 1) {
            return array(
                'code' => 5,
                'message' => 'This user is banned.'
            );

        }

        // Update the user
        $this->db->update('user', array(
            'login_fail' => 0,
            'date_login_fail' => false,
            'date_login' => DATETIME
        ));

        $this->session->set_userdata(array(
            'user_id'   => $user_id,
            'type'      => $type
        ));

        return array(
            'code' => 1,
            'message' => 'Login successful.'
        );

    }

    // ------------------------------------------------------------------------

    /**
    * Registers a user
    *
    * @param string $email
    * @param string $password
    * @param integer $activated
    * @param string $type
    *
    * @return boolean
    */
    public function register($email, $password, $activated, $type)
    {
        $key = hash('sha256', time() . $this->config->item('encryption_key'));
        $password_hash = hash('sha256', $password . $this->config->item('encryption_key'));

        $user_id = $this->user->insert(array(
            'email' => $email,
            'password' => $password_hash,
            'activated' => $activated,
            'key' => $key,
            'type' => $type,
            'date_login' => DATETIME,
            'date_added' => DATETIME
        ));

        if (!$user_id) {
            return 0;
        }

        $this->session->set_userdata(array(
            'user_id' => $user_id,
            'activated' => $activated,
            'email' => $email,
            'type' => $type
        ));

        return 1;
    }

    // ------------------------------------------------------------------------

    /**
    * Sends an email link to reset a password
    *
    * @param string $email
    *
    * @return boolean
    */
    public function forgot_password($email)
    {
        $this->db->select('user_id, key');
        $this->db->where('email', $email);
        $query = $this->db->get('user');
        $result = $query->row_array();

        if (empty($result)) {
            return 0;
        }

        // Send an email
        $this->content->send_email('forgot_password', $result['user_id'], array(
             '{{link}}' => site_url("index/change_password/{$result['user_id']}/{$result['key']}")
        ));

        return 1;
    }

    // ------------------------------------------------------------------------

    /**
    * User can change password after following reset email URL
    *
    * @param integer $user_id
    * @param string $key
    * @param string $password
    *
    * @return boolean
    */
    public function change_password($user_id, $key, $password)
    {
        $this->db->select('user_id');
        $this->db->where('key', $key);
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('user');
        $result = $query->row_array();

        if (empty($result)) {
            $this->output(0, 'Invalid key');
        }

        $password_hash = hash('sha256', $password . $this->config->item('encryption_key'));

        $this->db->where('user_id', $result['user_id']);
        $this->db->update('user', array(
            'password' => $password_hash,
            // Give the user a new key
            'key' => hash('sha256', time() . $this->config->item('encryption_key'))
        ));

        if ($this->db->affected_rows() > 0) {
            return 1;
        }
        return 0;
    }

    // ------------------------------------------------------------------------

}