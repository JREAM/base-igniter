<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends Base {

    // ------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata('user_id')) {
            redirect('client/login');
        }

        $this->data['title'] = 'Dashboard';
        $this->data['header'] = $this->load->view('client/inc/header_view', $this->data, true);
        $this->data['footer'] = $this->load->view('client/inc/footer_view', $this->data, true);
    }

    // ------------------------------------------------------------------------

    public function index()
    {
        $user_id = $this->session->userdata('user_id');

        $this->data['user'] = $this->user->meta->get_bundle($user_id);
        $this->load->view('client/dashboard_view', $this->data);
    }

    // ------------------------------------------------------------------------

}
