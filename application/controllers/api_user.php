<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_user extends Base {

    // ------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();
    }

    // ------------------------------------------------------------------------

    /**
    * Handles the login form and speaks to the user_model
    */
    public function do_login()
    {
        $email     = $this->input->post('email');
        $password  = $this->input->post('password');

        if ($this->input->post('user_type')) {
            $type = $this->input->post('user_type');
        } else {
            $type = 'client';
        }

        // Process the login
        $result = $this->user->login($email, $password, $type);
        if ($result['code'] != 1) {
            $this->output(0, $result);
        }

        $this->output(1, array('redirect' => $type . "/dashboard"));
    }

    // ------------------------------------------------------------------------

    /**
    * Destroys any session and logs a user out
    */
    public function do_logout()
    {
        $this->session->sess_destroy();
        redirect('/');
    }

    // ------------------------------------------------------------------------

    /**
    * Handles the registration form and speaks to the user_model
    */
    public function do_register()
    {
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[user.email]');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'matches[password]');

        if ($this->form_validation->run() == false) {
            $this->output(0, array('error' => $this->form_validation->error_array()));
        }

        $email      = $this->input->post('email');
        $password   = $this->input->post('password');
        $activated  = 1;
        $type       = 'client';

        $result = $this->user->register($email, $password, $activated, $type);

        if ($result == 0) {
            $this->output(0, 'Problem creating the user.');
        }
        $this->output(1, array('redirect' => 'client/dashboard'));
    }

    // ------------------------------------------------------------------------

    /**
    * Handles the forgot password form and speaks to the user_model
    */
    public function do_forgot_password()
    {
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        if ($this->form_validation->run() == false) {
            $this->output(0, $this->form_validation->error_array());
        }

        $email = $this->input->post('email');
        $result = $this->user->forgot_password($email);
        if ($result == 0) {
            $this->output(0, 'No user exists with these credentials.');
        }
        $this->output(1);
    }

    // ------------------------------------------------------------------------

    /**
    * Handles the change password form and speaks to the user_model
    */
    public function do_change_password()
    {
        if (!$this->session->userdata('user_id')) {
            redirect('client/login');
        }

        $this->form_validation->set_rules('password', 'Password', 'required|min_length[4]');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');
        $this->form_validation->set_rules('user_id', 'User ID', 'required');
        $this->form_validation->set_rules('key', 'Key', 'required|min_length[64]');

        if ($this->form_validation->run() == false) {
            $this->output(0, $this->form_validation->error_array());
        }

        $user_id = $this->input->post('user_id');
        $key     = $this->input->post('key');
        $password= $this->input->post('password');

        $result = $this->user->change_password($user_id, $key, $password);

        $this->output($result);
    }

    // ------------------------------------------------------------------------

    /**
    * Save a user's profile
    */
    public function do_profile()
    {
        if (!$this->session->userdata('user_id')) {
            redirect('client/login');
        }

        $user_id = $this->session->userdata('user_id');

        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');

        if ($this->form_validation->run() == false) {
            $this->output(0, $this->form_validation->error_array());
        }

        $first_name = $this->input->post('first_name');
        $last_name  = $this->input->post('last_name');
        $email      = $this->input->post('email');

        // Make sure a duplicate email isn't being used
        $total = $this->user->get_total(array(
            'email' => $email,
            'user_id !=' => $user_id
        ));

        if ($total != 0) {
            $this->output(0, 'This email is taken.');
        }

        // Update Profile
        $this->user->meta->set('profile', 'first_name', $first_name, $user_id);
        $this->user->meta->set('profile', 'last_name', $last_name, $user_id);
        $this->user->update(array(
            'email' => $email
        ), $user_id);

        $this->output(1);
    }
    // ------------------------------------------------------------------------

    /**
    * Save a user's alias
    */
    public function do_alias()
    {
        if (!$this->session->userdata('user_id')) {
            redirect('client/login');
        }

        $user_id = $this->session->userdata('user_id');

        $user = $this->user->get($user_id);
        if (isset($user[0]['alias']) && strlen($user[0]['alias']) > 0) {
            $this->output(0, array('alias' => 'Your alias is already set in stone.'));
        }

        $this->form_validation->set_rules('alias', 'Alias', 'required|alpha|is_unique[user.alias]|min_length[4]|max_length[16]');

        if ($this->form_validation->run() == false) {
            $this->output(0, $this->form_validation->error_array());
        }

        $alias = $this->input->post('alias');

        // Update Profile
        $this->user->update(array(
            'alias' => $alias
        ), $user_id);

        $this->output(1);
    }

    // ------------------------------------------------------------------------

    /**
    * Uploads an Avatar to S3
    *
    * @return string json
    */
    public function do_avatar()
    {
        if (!$this->session->userdata('user_id')) {
            redirect('client/login');
        }

        $user_id = $this->session->userdata('user_id');

        $upload = $this->content->upload($_FILES['avatar'], 'image');
        if ($upload[0] == 0) {
            $this->output($upload);
        }

        $resize = $this->content->make_thumbnail($upload[0]['file_name']);
        if ($resize[0] == 0) {
            $this->output($resize);
        }

        $this->content->upload_s3($upload[0]['file_name']);

        $this->user->meta->set('profile', 'avatar', $file_name, $user_id);
        $this->user->meta->set('profile', 'avatar_th', $file_name_th, $user_id);

        $this->output(1, array(
            'file_name' => $file_name,
            'file_name_th' => $file_name_th
        ));
    }

    // ------------------------------------------------------------------------

    /**
    * Removes an Avatar to S3
    *
    * @return string json
    */
    public function do_avatar_rm()
    {
        if (!$this->session->userdata('user_id')) {
            redirect('client/login');
        }

        $user_id = $this->session->userdata('user_id');

        $file_name = $this->user->meta->get('profile', 'avatar', $user_id);
        $file_name_th  = $this->user->meta->get('profile', 'avatar_th', $user_id);

        $this->content->delete_s3($file_name);
        $this->content->delete_s3($file_name_th);

        $this->user->meta->delete('profile', 'avatar', $user_id);
        $this->user->meta->delete('profile', 'avatar_th', $user_id);

        $this->output(1);
    }

    // ------------------------------------------------------------------------

}