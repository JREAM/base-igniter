<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends Base {

    // ------------------------------------------------------------------------

    public $data = array();

    // ------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();
    }

    // ------------------------------------------------------------------------

    /**
    * Loads the re-usable views for this controller
    * Should be called AFTER Settings $this->data['title'] or any others
    * to be parsed into those views.
    */
    private function _load_assets()
    {
        $this->data['header'] = $this->load->view('shared/inc/header_view', $this->data, true);
        $this->data['footer'] = $this->load->view('shared/inc/footer_view', $this->data, true);
    }

    // ------------------------------------------------------------------------

    public function index()
    {
        $this->data['title'] = 'Home';
        $this->_load_assets();
        $this->load->view('index_view', $this->data);
    }

    // ------------------------------------------------------------------------

    public function user()
    {
        $this->data['title'] = 'Users';
        $this->data['user'] = $this->user->meta->get_bundle();
        $this->_load_assets();
        $this->load->view('user_view', $this->data);
    }

    // ------------------------------------------------------------------------

    public function user_profile($alias)
    {
        $this->data['title'] = "Profile: $alias";
        $this->data['user'] = $this->user->meta->get_bundle(array('alias' => $alias));
        $this->_load_assets();

        if (empty($this->data['user'])) {
            redirect(site_url());
            exit;
        }

        $this->load->view('user_profile_view', $this->data);
    }

    // ------------------------------------------------------------------------

}
