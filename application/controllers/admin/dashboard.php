<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends Base {

    // ------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();
        $this->session->is_type('admin');

        $this->data['title'] = 'Admin Dashboard';
        $this->data['header'] = $this->load->view('admin/inc/header_view', $this->data, true);
        $this->data['footer'] = $this->load->view('admin/inc/footer_view', $this->data, true);
    }

    // ------------------------------------------------------------------------

    public function index()
    {
        $this->load->view('admin/dashboard_view', $this->data);
    }

    // ------------------------------------------------------------------------

}
