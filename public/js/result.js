var Result = function() {

    // ------------------------------------------------------------------------

    this.__construct = function() {
    };

    // ------------------------------------------------------------------------

    this.success = function(msg) {
        var dom = $("#success");
        var output = '';

        if (typeof msg === 'undefined') {
            output = "Success";
        }
        else {
            output = msg
        }

        $("#success").html(output);
        $("#success").show();
        setTimeout(function() {
            $("#success").fadeOut();
        }, 5000);

    };

    // ------------------------------------------------------------------------

    this.error = function(msg) {
        var dom = $("#error");
        var output = '';

        if (typeof msg == 'undefined') {
            output = "Error";
        }

        if (typeof msg == 'object') {
            for (var key in msg) {
                if (typeof msg[key] == 'object') {
                    for (var _key in msg[key]) {
                        output += msg[key][_key] + "<br />";
                    }
                } else {
                    output += msg[key] + "<br />";
                }
            }
        } else {
            output += msg;
        }

        $("#error").html(output)
        $("#error").show();
        setTimeout(function() {
            $("#error").fadeOut();
        }, 5000);

    };

    // ------------------------------------------------------------------------

    this.__construct();

};
